=======
Contact
=======

**E-Mail:**

* Stefanie Steinhauser: stefanie.steinhauser (at) lmu.de

**Further information:**

    * EnMAP mission, `www.enmap.org <http://www.enmap.org/>`_
    * LMU Department of Geography, `www.geographie.uni-muenchen.de <http://www.geo.lmu.de/geographie/en/>`_
