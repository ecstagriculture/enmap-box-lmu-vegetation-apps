.. |br| raw:: html

    <br>

========================
ANN Vegetation Processor
========================

About
-----

The ANN Vegetation Processor comprises artificial neural network (ANN) machine learning (ML) based on PROSAIL simulated spectra
(see :ref:`IVVRM <IVVRM>` and :ref:`LUT <LUT>`) to invert Leaf-Area-Index (LAI), Chlorophyll Content (Cab),
Leaf-Mass per Area, (LMA, Cm) and/or average leaf inclination angles (ALIA) (as currently
implemented) from a previously created look-up-table (LUT).
The workflow consists of |br|
(1) training of the ANN (Figure 1) and |br|
(2) applying the trained ANN to hyperspectral image data (Figure 2)

.. figure:: processor_img/ann_train.png

    Figure 1: The ANN Vegetation Processor Training Tool.

.. figure:: processor_img/ann_invert.png

    Figure 2: The ANN Vegetation Processor Inversion Tool.

Preparations
------------

Before using either ``ANN Training`` or ``ANN Inversion`` some prerequisites must be met regarding the input data: |br|

1. Hyperspectral imagery (surface reflectance) in ENVI .bsq format as integer type (e.g. reflectance * 10000).
   Wavelengths and wavelength units must be included in the .hdr header file and the data ignore value
   (no data value) must be known.
2. A LUT must have been created in advance using the tool ``Create Look-up-table``. The prepared LUT must have the
   exact same sensor specific spectral bands setup as the imagery data on which ANN inversion will be applied upon.
   The ``Multiplication Factor`` for LUT-Creation must be set to 10000. Mind also, that bands at wavelengths <400 nm
   and >2500 nm (permitted PROSAIL range) must be deleted from the input imagery beforehand.

.. note:: A pretrained ANN will be available for EnMAP imagery as soon as EnMAP data will be available. The currently
   included ANN model (EnMAP.meta) applies for simulated EnMAP data.

ANN Training
--------------------------

1. Start ``ANN Training`` from :menuselection:`Applications --> Agricultural Applications --> ANN Training` |br|
   (Figure 1).
2. Select a previously created LUT (.lut file).
3. Choose which bands to exclude from ANN training (water vapor absorption bands are excluded by default).
4. Apply Principal Component Analysis (PCA) and set the number of components to reduce dimensionality and collinearity
   issues (recommended).
5. Assign output directory and output model name.
6. Click :guilabel:`Run` to start calculation.

ANN Inversion
-------------

1. Start ``ANN Inversion`` from :menuselection:`Applications --> Agricultural Applications --> ANN Training` |br|
   (Figure 2).
2. Select a previously trained ANN model (.meta file). If not yet done, `Train new model` connects back to
   ``ANN Training`` tool
3. Choose hyperspectral input imagery.
4. (Optional) Either select a mask image to restrict processed image pixels (mask image format: binary integer raster
   with pixel values = 1: pixels to process; pixel values = 0: pixels to exclude) or exclude pixels by
   Normalized-Difference-Vegetation-Index (NDVI) threshold (only pixels above threshold will be processed).
5. Either select 3-band sun-sensor-target imagery
   (band 1: sun zenith angle (SZA); band 2: observer zenith angle; band 3: relative azimuth angle (rAA) or use single
   preset angles.
6. Choose variables to invert
7. Select if output should be saved as a multiband raster or as multiple single band raster files.
8. Click :guilabel:`Run` to start inversion.



