.. |br| raw:: html

    <br>

===========================
Plant Water Retrieval (PWR)
===========================

About
-----

The Plant Water Retrieval (PWR) tool uses is an efficient algorithm to directly extract quantitative
water content information in [|xxx|] or [cm] from hyperspectral images. It applies the
Beer-Lambert law [1] to inversely determine the optical thickness *d* of the water layer responsible for
the water absorption feature at 970 nm using water absorption coefficients :math:`{\alpha}` for pure
liquid water. :math:`{\Phi}` |xxy| denotes the incident radiation intensity and :math:`{\Phi}` is
the attenuated radiation intensity.

.. |xxx| replace:: g cm\ :sup:`-2`
.. |xxy| replace:: \ :sub:`0`

+-----------+--------------------------+---+
| .. figure:: pwr_img/lambert_beer.PNG |[1]|
+-----------+--------------------------+---+

After calibration over various validation datasets (refer to `Wocher et al. (2018) <https://www.mdpi.com/2072-4292/10/12/1924>`_ for detailed information)
the final PWR equation [2] is as follows:

+-----------+--------------------------+---+
| .. figure:: pwr_img/pwr_eq.PNG       |[2]|
+-----------+--------------------------+---+

with *R* |xxz| being the measured reflectance. The algorithm stops when the sum of the residuals to
the assumed dry reflectance (Figure 1) is reached.

.. |xxz| replace:: \ :sub:`0`

.. figure:: pwr_img/pwr_example.PNG

    Figure 1: Determination of optically active water thickness *d* from a measured spectrum through
    minimization of residuals to assumed dry reflectance line (dotted line).

Getting Started
---------------

1. Select a GDAL readable hyperspectral input image via the :guilabel:`...` button and define a location
   for the output image :guilabel:`...`.
2. If you need to speed up the calculation a ``NDVI threshold`` for vegetation pixels can be assigned.
3. Select an ``Output No Data`` value for the output image.
4. If your input image is of integer type, assign a division factor to obtain relative reflectances expected
   by the algorithm.
5. Click :guilabel:`Run` to start the plant water retrieval algorithm.
