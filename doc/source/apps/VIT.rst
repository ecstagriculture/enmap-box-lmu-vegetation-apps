================================
Vegetation Indices Toolbox (VIT)
================================

About
-----

The Vegetation Indices Toolbox (VIT) is a collection of a vast variety of spectral indices that have been published in the last years.
While most spectral indices are normalized difference indices (NDI), also simple indices (SI) and multiband indices have been included in the VIT library.

.. tip:: Move the mouse cursor over indices until the tooltips appear. The tooltips show the according literature reference.

.. figure:: vit_img/vit.png

    Figure 1: Screenshot of the Vegetation Indices Toolbox graphical user interface with some selected indices to be calculated.

User Input
----------

Image Files
~~~~~~~~~~~

**Input-Image**: Select a GDAL readable input raster that contains information about surface reflectance.

**Output-Image**: Define a output-raster basename.

Image Out
~~~~~~~~~

If multiple indices have been selected they will either be stacked in a multi-band raster image (**Output to single file**) or one single band
file per selected index will be saved with index-suffices added to the output-raster basename (**Output to individual files**).

.. tip:: Using the buttons :guilabel:`Select All` in combination with ``Output to single file``, all available indices will
    be stored in one large multi-band raster image.

Interpolation Type
~~~~~~~~~~~~~~~~~~

In case a selected index demands a specific wavelength band that the loaded input image does not contain, either the closest
band will be used (**Nearest Neighbor**) or an interpolation between the nearest available bands will be performed (**Linear or IDW**).

No Data
~~~~~~~

Make sure both the input and the output images have no data values assigned.

List of Indices
---------------

The following tables list all the indices available in the Vegetation Index Toolbox. The Indices are categorized according to their sensitivity to
vegetation structural, biochemical, or biophysical variables.

.. figure:: vit_img/VIT_indizes_1.png

.. figure:: vit_img/VIT_indizes_2.png

.. figure:: vit_img/VIT_indizes_3.png

.. figure:: vit_img/VIT_indizes_4.png

.. figure:: vit_img/VIT_indizes_5.png

.. figure:: vit_img/VIT_indizes_6.png

.. figure:: vit_img/VIT_indizes_7.png

.. figure:: vit_img/VIT_indizes_8.png

    Figure 2: Complete list of indices available in the Vegetation Indices Toolbox


Click :guilabel:`OK` to start the indices calculation.