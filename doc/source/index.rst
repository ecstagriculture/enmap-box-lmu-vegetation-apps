.. LMU Vegetation Apps documentation master file, created by
   sphinx-quickstart on Wed Jun 13 11:56:15 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. |br| raw:: html

    <br>

EnMAP-Box 3 LMU Agricultural Apps Documentation
===============================================

|

**EnMAP and Agriculture:**

EnMAP enables the derivation of special information
products, which are used for decision-
making in the context of site-specific
crop production.
Implementing precision technology in farming
strategies can lead to more ecologically
and economically sustainable utilization
of the bioproductive land surface.
Compared to conventional multispectral
Earth observation systems, hyperspectral
imaging systems such as EnMAP guarantee
(1) a greater variety of observable variables,
(2) higher accuracy of the information
products by avoiding misinterpretations,
and (3) global transferability of variable
estimation techniques, which are independent
from in situ calibration data.

**EnMAP-Box 3 Agricultural Applications:**

In order to use IVVRM, the EnMAP-Box 3.0 or higher needs to be downloaded and properly installed as QGIS-plugin.
Please visit `EnMap-Box 3 documentation <https://enmap-box.readthedocs.io/en/latest/index.html>`_ for further instructions
on how to install and link EnMAP-Box 3.0 with QGIS.

**Related websites**

- Environmental Mapping and Analysis Program (EnMAP): `www.enmap.org <http://www.enmap.org/>`_
- EnMAP-Box 3 GitHub source code repository: `https://github.com/EnMAP-Box/enmap-box <https://github.com/EnMAP-Box/enmap-box>`_
- EnMAP-Box 3 LMU Vegetation Apps source code repository: |br| `https://bitbucket.org/ecstagriculture/enmap-box-lmu-vegetation-apps/ <https://bitbucket.org/ecstagriculture/enmap-box-lmu-vegetation-apps/>`_

|

**This documentation is structured as follows:**

.. toctree::
   :maxdepth: 2
   :caption: General:

   general/contact.rst

.. toctree::
   :maxdepth: 3
   :caption: Applications:

   apps/IVVRM.rst
   apps/LUT.rst
   apps/VIT.rst
   apps/PWR.rst
   apps/ASI.rst
   apps/REIP.rst
   apps/ANN_Processor.rst
   apps/Hybrid_Retrieval_Workflow.rst


.. toctree::
   :maxdepth: 2
   :caption: Tutorials:

   tutorials/IVVRM_tut.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
