# coding=utf-8

import unittest
from qgis import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from enmapbox.testing import initQgisApplication
from enmapbox.gui.utils import *
QGIS_APP = initQgisApplication()




class TestCases(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass
    def setUp(self):

        pass

    def tearDown(self):
        pass

    def start_GUI_IVVRM(self, *args):
        from lmuvegetationapps.IVVRM.IVVRM_GUI import MainUiFunc
        m = MainUiFunc()
        m.show()
        # #the the EnMAP-Box know if you create any new file
        # gui1.sigFileCreated.connect(self.enmapbox.addSource)

    def test_start_GUI_LUT(self, *args):
        from lmuvegetationapps.LUT.CreateLUT_GUI import MainUiFunc
        m = MainUiFunc()
        m.show()

    def test_start_GUI_Inv(self, *args):
        from lmuvegetationapps.LUT.InvertLUT_GUI import MainUiFunc
        m = MainUiFunc()
        m.show()

    def test_start_GUI_VIT(self, *args):
        from lmuvegetationapps.VIT.VIT_GUI import MainUiFunc
        m = MainUiFunc()
        m.show()

    def test_start_GUI_PWR(self, *args):
        from lmuvegetationapps.PWR.PWR_GUI import MainUiFunc
        m = MainUiFunc()
        m.show()

    def test_createApp(self):
        from enmapbox.gui.enmapboxgui import EnMAPBox
        import enmapbox.gui
        enmapbox.gui.LOAD_PROCESSING_FRAMEWORK = False
        enmapbox.gui.LOAD_INTERNAL_APPS = False
        enmapbox.gui.LOAD_EXTERNAL_APPS = False
        EB = EnMAPBox()
        from lmuvegetationapps import enmapboxApplicationFactory
        from enmapbox.gui.applications import EnMAPBoxApplication, ApplicationWrapper
        app = enmapboxApplicationFactory(EB)

        self.assertIsInstance(app, list)
        for a in app:
            self.assertIsInstance(a, EnMAPBoxApplication)

            EB.addApplication(a)
            self.assertTrue(a in EB.applicationRegistry.applications())

            applicationWrapper = EB.applicationRegistry.applicationWrapper(a)
            self.assertIsInstance(applicationWrapper, list)
            self.assertTrue(len(applicationWrapper), 1)
            applicationWrapper = applicationWrapper[0]
            self.assertIsInstance(applicationWrapper, ApplicationWrapper)

            #calls all QMenu actions
            def triggerActions(menuItem):
                if isinstance(menuItem, QAction):
                    menuItem.trigger()
                elif isinstance(menuItem, QMenu):
                    for a in menuItem.actions():
                        triggerActions(a)

            for menuItem in applicationWrapper.menuItems:
                triggerActions(menuItem)



class TestCasesEnMAPBox(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass
    def setUp(self):

        pass


if __name__ == "__main__":
    unittest.main()



